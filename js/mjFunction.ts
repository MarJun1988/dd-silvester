$(function () {
    interface AtomClock {
        abbreviation: string,
        client_ip: string,
        datetime: string,
        day_of_week: number,
        day_of_year: number,
        dst: boolean,
        dst_from: null,
        dst_offset: number,
        dst_until: null,
        raw_offset: number,
        timezone: number,
        unixtime: 1641021460,
        utc_datetime: string,
        utc_offset: string,
        week_number: number
    }

    /* Abrufen der aktuellen Atomuhrzeit */
    let currentTime: Promise<AtomClock> = fetch(`https://worldtimeapi.org/api/timezone/Europe/Berlin.json`)
        .then(res => res.json())
        .then((res: AtomClock) => {
            return res;
        });

    /* Festlegung des Countdown-Datums und Uhrzeit */
    const countData: any = new Date('jan 1, 2022 00:00:00').getTime();
    /* Elemente wo die Zeiten Dargestellt werden des Counters */
    const viewCount: JQuery = $('#mjCountdown');
    const viewCountDays: JQuery = $('#countDays');
    const viewCountHours: JQuery = $('#countHours');
    const viewCountMinutes: JQuery = $('#countMinutes');
    const viewCountSeconds: JQuery = $('#countSeconds');

    /* Sekunden Blinken */
    const secondCountFlash: boolean = true;
    /* Element was blinken soll */
    const secondCountElementFlash: JQuery = $('.container');

    /* Elemente wo die aktuelle Uhrzeit und Datum angezeigt wird*/
    const viewCurrent: JQuery = $('#mjCurrentDateTime');
    const viewCurrentDays: JQuery = $('#currentDay');
    const viewCurrentHours: JQuery = $('#currentHours');
    const viewCurrentMinutes: JQuery = $('#currentMinute');
    const viewCurrentSeconds: JQuery = $('#currentSecond');

    /* Sekunden Blinken */
    const secondCurrentFlash: boolean = true;
    /* Element was blinken soll */
    const secondCurrentElementFlash: JQuery = $('.container');

    /* Happy NewYear Element */
    const viewHappyNewYear: JQuery = $('#mjHappyNewYear');

    /* Funktion des Counters */
    function getCountdown() {
        /* Aktuelles Datum und Uhrzeit*/
        let now: any = new Date().getTime();
        /* Berechnung der Differenz */
        let diff: number = countData - now;
        /* Berechnung der Zeiten */
        let second: number = 1000;
        let minute: number = second * 60;
        let hour: number = minute * 60;
        let day: number = hour * 24;
        /* Berechnung der Differenz zur Anzeige */
        let d: number = Math.floor(diff / day);
        let h: number = Math.floor((diff % day) / hour);
        let m: number = Math.floor((diff % hour) / minute);
        let s: number = Math.floor((diff % minute) / second);
        /* Darstellung auf den Elementen */
        viewCountDays.text(d);
        viewCountHours.text(h);
        viewCountMinutes.text(m);
        viewCountSeconds.text(s);
        /* Sekunden blinken */
        if (secondCountFlash) {
            if (s % 2 == 0) {
                secondCountElementFlash.addClass('secondBoom');
            } else {
                secondCountElementFlash.removeClass('secondBoom');
            }
        }
    }

    /* Funktion der aktuellen Uhrzeit */
    function getCurrentTime() {
        /* Aktuelles Datum und Uhrzeit*/
        const second: number = new Date().getSeconds();
        const minute: number = new Date().getMinutes();
        const hour: number = new Date().getHours();
        const day: number = new Date().getDate();

        /* Die erste Minute Happy New Year Anzeigen */
        if (day === 1 && hour === 0 && minute === 0 && second < 59) {
            viewCount.hide();
            viewCurrent.hide();
            viewHappyNewYear.show();
        } else {
            /* Darstellung auf den Elementen */
            viewCurrentDays.text(day - 1);
            viewCurrentHours.text(hour);
            viewCurrentMinutes.text(minute);
            viewCurrentSeconds.text(second);
            /* Sekunden blinken */
            if (secondCurrentFlash) {
                if (second % 2 == 0) {
                    secondCurrentElementFlash.addClass('secondBoom');
                } else {
                    secondCurrentElementFlash.removeClass('secondBoom');
                }
            }
        }
    }

    /* Starten der Anzeige (interval alle 1 Sekunden) */
    setInterval(function () {
        /* Aktuelles Datum und Uhrzeit*/
        let now: number = new Date().getTime();
        /* Berechnung der Differenz */
        let diff: number = countData - now;

        if (diff < 0) {
            viewCount.hide();
            viewHappyNewYear.hide();
            viewCurrent.show();
            getCurrentTime();
        } else {
            viewCurrent.hide();
            viewCount.show();
            getCountdown();
        }
    }, 1000);
});