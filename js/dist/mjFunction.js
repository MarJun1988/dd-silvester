"use strict";
$(function () {
    /* Abrufen der aktuellen Atomuhrzeit */
    let currentTime = fetch(`https://worldtimeapi.org/api/timezone/Europe/Berlin.json`)
        .then(res => res.json())
        .then((res) => {
        return res;
    });
    /* Festlegung des Countdown-Datums und Uhrzeit */
    const countData = new Date('jan 1, 2022 00:00:00').getTime();
    /* Elemente wo die Zeiten Dargestellt werden des Counters */
    const viewCount = $('#mjCountdown');
    const viewCountDays = $('#countDays');
    const viewCountHours = $('#countHours');
    const viewCountMinutes = $('#countMinutes');
    const viewCountSeconds = $('#countSeconds');
    /* Sekunden Blinken */
    const secondCountFlash = true;
    /* Element was blinken soll */
    const secondCountElementFlash = $('.container');
    /* Elemente wo die aktuelle Uhrzeit und Datum angezeigt wird*/
    const viewCurrent = $('#mjCurrentDateTime');
    const viewCurrentDays = $('#currentDay');
    const viewCurrentHours = $('#currentHours');
    const viewCurrentMinutes = $('#currentMinute');
    const viewCurrentSeconds = $('#currentSecond');
    /* Sekunden Blinken */
    const secondCurrentFlash = true;
    /* Element was blinken soll */
    const secondCurrentElementFlash = $('.container');
    /* Happy NewYear Element */
    const viewHappyNewYear = $('#mjHappyNewYear');
    /* Funktion des Counters */
    function getCountdown() {
        /* Aktuelles Datum und Uhrzeit*/
        let now = new Date().getTime();
        /* Berechnung der Differenz */
        let diff = countData - now;
        /* Berechnung der Zeiten */
        let second = 1000;
        let minute = second * 60;
        let hour = minute * 60;
        let day = hour * 24;
        /* Berechnung der Differenz zur Anzeige */
        let d = Math.floor(diff / day);
        let h = Math.floor((diff % day) / hour);
        let m = Math.floor((diff % hour) / minute);
        let s = Math.floor((diff % minute) / second);
        /* Darstellung auf den Elementen */
        viewCountDays.text(d);
        viewCountHours.text(h);
        viewCountMinutes.text(m);
        viewCountSeconds.text(s);
        /* Sekunden blinken */
        if (secondCountFlash) {
            if (s % 2 == 0) {
                secondCountElementFlash.addClass('secondBoom');
            }
            else {
                secondCountElementFlash.removeClass('secondBoom');
            }
        }
    }
    /* Funktion der aktuellen Uhrzeit */
    function getCurrentTime() {
        /* Aktuelles Datum und Uhrzeit*/
        const second = new Date().getSeconds();
        const minute = new Date().getMinutes();
        const hour = new Date().getHours();
        const day = new Date().getDate();
        /* Die erste Minute Happy New Year Anzeigen */
        if (day === 1 && hour === 0 && minute === 0 && second < 59) {
            viewCount.hide();
            viewCurrent.hide();
            viewHappyNewYear.show();
        }
        else {
            /* Darstellung auf den Elementen */
            viewCurrentDays.text(day - 1);
            viewCurrentHours.text(hour);
            viewCurrentMinutes.text(minute);
            viewCurrentSeconds.text(second);
            /* Sekunden blinken */
            if (secondCurrentFlash) {
                if (second % 2 == 0) {
                    secondCurrentElementFlash.addClass('secondBoom');
                }
                else {
                    secondCurrentElementFlash.removeClass('secondBoom');
                }
            }
        }
    }
    /* Starten der Anzeige (interval alle 1 Sekunden) */
    setInterval(function () {
        /* Aktuelles Datum und Uhrzeit*/
        let now = new Date().getTime();
        /* Berechnung der Differenz */
        let diff = countData - now;
        if (diff < 0) {
            viewCount.hide();
            viewHappyNewYear.hide();
            viewCurrent.show();
            getCurrentTime();
        }
        else {
            viewCurrent.hide();
            viewCount.show();
            getCountdown();
        }
    }, 1000);
});
//# sourceMappingURL=mjFunction.js.map